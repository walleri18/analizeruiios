//
//  ContentView.swift
//  Analizer iOS
//
//  Created by Туров Виталий on 26.06.2020.
//  Copyright © 2020 TurovVitaliy. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
